import argparse
from pathlib import Path

import pandas as pd
import numpy as np
from sklearn.model_selection import KFold
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
from scipy.stats import spearmanr
import tqdm

from beataml_sc1 import utils


# CLI
parser = argparse.ArgumentParser()
parser.add_argument('-md', '--max-depth', type=int, required=True,
                    help='Max depth of the random forest.')
parser.add_argument('-ne', '--n-estimators', type=int, required=True,
                    help='Number of estimators of the random forest.')
parser.add_argument('-ns', '--n-splits', type=int, required=True,
                    help='Number of splits in k-fold validation strategy.')
parser.add_argument('-ifid', '--input-file-input-data', type=str, required=True,
                    help='Input file for input data.')
parser.add_argument('-ifod', '--input-file-output-data', type=str, required=True,
                    help='Input file for output data.')
parser.add_argument('-of', '--output-file', type=str, required=True,
                    help='Output file for results.')
parser.add_argument('-nj', '--n-jobs', type=int, required=True,
                    help='Number of cores to use.')
parser.add_argument('-s', '--silent', action='store_true',
                    help='Operate this script in silent mode.')
parser.add_argument('-mf', '--model-folder', type=str, default='', required=False,
                    help='If given, models are saved in this folder.')
args = parser.parse_args()
max_depth = args.max_depth
n_estimators = args.n_estimators
n_splits = args.n_splits
input_file_input_data = args.input_file_input_data
input_file_output_data = args.input_file_output_data
output_file = args.output_file
n_jobs = args.n_jobs
silent = args.silent
model_folder = args.model_folder

# Load data
df_input = pd.read_csv(input_file_input_data)
df_output = pd.read_csv(input_file_output_data)

# Pre-process data: Remove forbidden input columns
forbidden_column_names = [
                                'k.RNAseq=23', # Forbidden column
                                'UNKNOWN',     # Columns called "UNKNOWN"
                                'None',        # Columns with "None"
                         ]
columns_to_remove = []
for col in df_input.columns:
    for c_forbidden in forbidden_column_names:
        if c_forbidden in col:
            columns_to_remove.append(col)
for col in columns_to_remove:
    del df_input[col]

# Computation
df_results = pd.DataFrame(columns=['inhibitor', 'fold', 'metric', 'max_depth', 'n_estimators', 'value'])
for i_target_column_name, target_column_name in tqdm.tqdm(enumerate(df_output.columns), disable=silent):

    available_data = df_output[target_column_name] != 0.0

    df_input_available = df_input[available_data]
    df_output_available = df_output[available_data]

    df_input_available.index = np.arange(len(df_input_available))
    df_output_available.index = np.arange(len(df_output_available))

    y = df_output_available[target_column_name].values
    X = df_input_available.values

    kf = KFold(n_splits=n_splits, shuffle=True, random_state=0)

    for i_nsplits, (train_index, test_index) in enumerate(kf.split(X)):

        assert len(list(set(train_index) & set(test_index))) == 0, 'training and test data must not overlap'

        # Train and test split
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]

        # Train model
        regr = RandomForestRegressor(max_depth=max_depth, n_estimators=n_estimators,
                                     random_state=0, n_jobs=n_jobs)

        regr.fit(X_train, y_train)
        y_test_predicted = regr.predict(X_test)

        c_R2 = regr.score(X_test, y_test)
        mse = mean_squared_error(y_test, y_test_predicted)
        spearman = spearmanr(y_test, y_test_predicted).correlation
        data_tmp = {
                        'inhibitor':    [i_target_column_name, i_target_column_name, i_target_column_name,],
                        'fold':         [i_nsplits, i_nsplits, i_nsplits,],
                        'metric':       ['c_R2', 'mse', 'spearman',],
                        'max_depth':    [max_depth, max_depth, max_depth,],
                        'n_estimators': [n_estimators, n_estimators, n_estimators],
                        'value':        [c_R2, mse, spearman,],
                    }

        df_append = pd.DataFrame.from_dict(data_tmp)
        df_results = pd.DataFrame.append(df_results, df_append, ignore_index=True)

        if len(model_folder) > 0:
            utils.save_random_forest_model(Path(model_folder) / f'iTargetColumnName{i_target_column_name}_iNsplits{i_nsplits}.model', regr)

# Save
df_results.to_csv(output_file, index=False)
