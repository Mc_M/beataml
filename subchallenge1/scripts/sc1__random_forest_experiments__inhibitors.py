import argparse
from pathlib import Path

import pandas as pd
import numpy as np
from sklearn.model_selection import KFold
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
from scipy.stats import spearmanr
import tqdm

from beataml_sc1 import utils


# CLI
parser = argparse.ArgumentParser()
parser.add_argument('-md', '--max-depth', type=int, required=True,
                    help='Max depth of the random forest.')
parser.add_argument('-ne', '--n-estimators', type=int, required=True,
                    help='Number of estimators of the random forest.')
parser.add_argument('-ns', '--n-splits', type=int, required=True,
                    help='Number of splits in k-fold validation strategy.')
parser.add_argument('-if', '--input-file', type=str, required=True,
                    help='Input file for data.')
parser.add_argument('-of', '--output-file', type=str, required=True,
                    help='Output file for results.')
parser.add_argument('-nj', '--n-jobs', type=int, required=True,
                    help='Number of cores to use.')
parser.add_argument('-s', '--silent', action='store_true',
                    help='Operate this script in silent mode.')
parser.add_argument('-mf', '--model-folder', type=str, default='', required=False,
                    help='If given, models are saved in this folder.')
args = parser.parse_args()
max_depth = args.max_depth
n_estimators = args.n_estimators
n_splits = args.n_splits
input_file = args.input_file
output_file = args.output_file
n_jobs = args.n_jobs
silent = args.silent
model_folder = args.model_folder

# Load data
df = pd.read_csv(input_file)
signature_columns = np.array(['Signatur' in column_name for column_name in df.columns])
inhibitor_columns = np.logical_not(signature_columns)
signature_columns[22] = False # Exclude index 22 as I was told

# Computation
df_output = pd.DataFrame(columns=['inhibitor', 'fold', 'metric', 'max_depth', 'n_estimators', 'value'])
for target_inhibitor in tqdm.tqdm(range(np.count_nonzero(inhibitor_columns)), disable=silent):

    target_column_name = f'Inhibitor{target_inhibitor}'

    available_data = df[target_column_name] != 0.0
    df_available = df[available_data]

    y = df_available[target_column_name]

    X = df_available[df_available.columns[signature_columns]]

    y = np.array(y)
    X = np.array(X)

    kf = KFold(n_splits=n_splits, shuffle=True, random_state=0)

    for i_nsplits, (train_index, test_index) in enumerate(kf.split(X)):

        assert len(list(set(train_index) & set(test_index))) == 0, 'training and test data must not overlap'

        # Train and test split
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]

        # Train model
        regr = RandomForestRegressor(max_depth=max_depth, n_estimators=n_estimators,
                                     random_state=0, n_jobs=n_jobs)

        regr.fit(X_train, y_train)
        y_test_predicted = regr.predict(X_test)

        c_R2 = regr.score(X_test, y_test)
        mse = mean_squared_error(y_test, y_test_predicted)
        spearman = spearmanr(y_test, y_test_predicted).correlation
        data_tmp = {
                        'inhibitor':    [target_inhibitor, target_inhibitor, target_inhibitor,],
                        'fold':         [i_nsplits, i_nsplits, i_nsplits,],
                        'metric':       ['c_R2', 'mse', 'spearman',],
                        'max_depth':    [max_depth, max_depth, max_depth,],
                        'n_estimators': [n_estimators, n_estimators, n_estimators],
                        'value':        [c_R2, mse, spearman,],
                    }

        df_append = pd.DataFrame.from_dict(data_tmp)
        df_output = pd.DataFrame.append(df_output, df_append, ignore_index=True)

        if len(model_folder) > 0:
            utils.save_random_forest_model(Path(model_folder) / f'iTargetColumnName{target_inhibitor}_iNsplits{i_nsplits}.model', regr)

# Save
df_output.to_csv(output_file, index=False)
