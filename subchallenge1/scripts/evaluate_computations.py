import argparse
from pathlib import Path

import pandas as pd
import numpy as np
import tqdm
import matplotlib.pyplot as plt


# CLI
parser = argparse.ArgumentParser()
parser.add_argument('-if', '--input-folder', type=str, required=True,
                    help='Input file for data.')
parser.add_argument('-of', '--output-folder', type=str, required=True,
                    help='Output file for results.')
parser.add_argument('-s', '--silent', action='store_true',
                    help='Operate this script in silent mode.')
args = parser.parse_args()
input_folder = Path( args.input_folder )
output_folder = Path( args.output_folder )
silent = args.silent

# Load data
dfs = [pd.read_csv(x) for x in input_folder.glob('*.dat')]
df = pd.concat(dfs, ignore_index=True)

# Metrices to evalute
metrics = {
                'c_R2': lambda x: np.max(x),
                'mse': lambda x: np.min(x),
                'spearman': lambda x: np.max(x),
            }
metrics_arg = {
                'c_R2': lambda x: np.argmax(x),
                'mse': lambda x: np.argmin(x),
                'spearman': lambda x: np.argmax(x),
            }
metric_to_index = { xx:ii for ii, xx in enumerate(list(metrics.keys())) }

# Compute mean over folds
df = df.groupby(['metric', 'inhibitor', 'max_depth', 'n_estimators'])['value'].mean()

# Compute performance w.r.t. metrics
data_plot = {'metric': [], 'inhibitor': [], 'performance': []}
df_metric_inhibitor = df.groupby(['metric', 'inhibitor'])
for key, value in df_metric_inhibitor:

    metric = key[0]
    inhibitor = key[1]
    performance = metrics[metric](value)
    data_plot['metric'].append(metric)
    data_plot['inhibitor'].append(inhibitor)
    data_plot['performance'].append(performance)

df_plot = pd.DataFrame.from_dict(data_plot)

# Plot
fig, axes = plt.subplots(len(metrics), 1, figsize=[15, 8], sharex=True)

for metric in metrics:

    # Prepare data
    condition_metric = df_plot['metric'] == metric
    plot_x = df_plot[condition_metric]['inhibitor']
    plot_y = df_plot[condition_metric]['performance']

    # Plotting destination
    i_ax = metric_to_index[metric]
    ax = axes[i_ax]

    # Plotting
    ax.plot(plot_x, plot_y)
    ax.axhline(np.mean(plot_y), ls='-', c='red', label=f'mean = {np.mean(plot_y)}')
    ax.fill_between(plot_x,
                    np.mean(plot_y)-np.std(plot_y),
                    np.mean(plot_y)+np.std(plot_y),
                    color='red', alpha=0.3, label=f'std = {np.std(plot_y)}')

    ax.set_title(metric)
    ax.legend()

axes[-1].set_xlabel('Inhibitor number that is predicted')

plt.savefig(output_folder / 'performance_plot.png')

# Compute best hyperparameters
data_best_hyperparameters = {'metric': [], 'inhibitor': [], 'best_max_depth': [], 'best_n_estimators': []}
df_metric_inhibitor = df.groupby(['metric', 'inhibitor'])
for key, value in df_metric_inhibitor:

    metric = key[0]
    inhibitor = key[1]

    best_hyper_parameters = metrics_arg[metric](value)

    _, _, best_max_depth, best_n_estimators = value.index[best_hyper_parameters]

    data_best_hyperparameters['metric'].append(metric)
    data_best_hyperparameters['inhibitor'].append(inhibitor)
    data_best_hyperparameters['best_max_depth'].append(best_max_depth)
    data_best_hyperparameters['best_n_estimators'].append(best_n_estimators)

df_best_hyperparameters = pd.DataFrame.from_dict(data_best_hyperparameters)
df_best_hyperparameters.to_csv(output_folder / 'best_hyper_parameters.dat', index=False)
