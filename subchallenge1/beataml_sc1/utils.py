"""
Utility package.
"""

import pickle


def save_random_forest_model(file_path, model):
    """
    Parameters:
    -----------

    file_path : str
        The file path to save the model as.
    model : sklearn.ensemble.RandomForestRegressor
        The random forest model to save.
    """
    with open(file_path, 'wb') as f:
        pickle.dump(model, f)

def load_random_forest_model(file_path):
    """
    Parameters:
    -----------

    file_path : str
        The file path to load the model from.
    """
    with open(file_path, 'rb') as f:
        model = pickle.load(f)
    return model
