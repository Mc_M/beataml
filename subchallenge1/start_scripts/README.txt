README
======

  * To compute the inhibitor predictions execute: ``snakemake --cores <nr_cores, multiple of 4> -p predict_inhibitors__compute``.
  * To compute the inhibitor signature predictions execute: ``snakemake --cores <nr_cores, multiple of 4> -p predict_inhibitor_signatures__compute``.
