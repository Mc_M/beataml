# README #

This repository contains my contributions to the BeatAML challenge in the team "GrauLab" by Michael Grau et al.

## Installation ##

All the following commands are executed in the subfolder ``subchallenge1``:

1. Create an environment: ``conda create --name beataml python=3``.
2. Install all dependencies using ``pip install -r requirements.txt``.
3. Install the package in development mode with ``pip install -e .`` (do not forget the dot, '.').

### Usage ###

1. Activate environment: ``. activate beataml``.
2. Then you can import packages from ``beataml_sc1``.
